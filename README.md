# Assignment 6 

This repository includes SQL scripts to create a Superhero database 
and a console application in C# that acces daata from a database (Chinook).

## Table of Contents
- [Installation](#installation)
- [Usage](#usage)
- [Description](#description)
- [Maintainers](#maintainers)
- [Project status](#project-status)


## Installation

Install following tools:

* Visual Studio 2019 with .NET 5
* SQL Server Managment Studio

## Usage

First clone the git repository

    git@gitlab.com:josteinskadberg/assignment6.git

### Using the SQL scripts
* Open SQLScripts in SQL Server Managment Studio
* Run the script in correct order, start with 01, 02, 03, .....,  09


### Using the console application in C#
* Open in Visual Studio 2019 by clicking the ***RepositoryImplementation.sln*** file
* Run program with button with the *green arrow and RepositoryImplementation* or *F5*


## Description

There are two different parts in this repository. First part inkludes some SQL Scripts to create a Superhero database.
Second part is a console application in C# that deals with manipulating SQL Server data in Visual Studio 
using a library called SQL Client, and the database that where used is called *Chinook*

### First part - SQL Scripts

The scripts to create SuperheroDb.

#### Three Tables:
***Superhero*** - Autoincremented integer ID, Name, Alias and Origin.

***Assistant*** - Autoincremented integer ID, Name.

***Power*** - Autoincremented integer ID, Name, Description.

#### The scripts:

***01_dbCreate.sql:***
    - Creating the SuperherosDB

***02_tableCreate.sql:***
    - Creating all of the tables listed above with primary keys.

***03_relationshipSuperheroAssistant.sql:***
    - Alter Assistent Table to add forgein key to the Superhero table (One to Many relation).

***04_relationshipSuperheroPower.sql:***
    - Creating a new table to manage forgein keys between Power table with Superhero table (Many to Many relation).

***05_insertSuperheros.sql:***
    - Inserts three new Superheros into the database.

***06_insertAssistant.sql:***
    - Inserts three new Assistants to the database.

***07_powers.sql:***
    - Inserts four new Powers to the database.

***08_updateSuperhero.sql:***
    - Updates a superheros name.

***09_deleteAssistant.spwl.sql:***
    - Deletes an assistant.

#### Visualing SuperherosDb

![SuperheroDb](/Images/SuperheroDb.png)


### Second part - Console application in C#

In this part the *Chinook* database has been interacted with C# in Visual Studio.
Chinook models the iTunes database of cusomers purchasing songs.

#### Models
***Customer*** - Customer class to structure the customer data

***CountryCustomerCount*** - CountryCustomerCount class to structure the data needed to return number of customers in each cuntry

***CustomerSpend*** - CustomerSpend class to structure the data needed to retrun the custumers who are highest spenders from high to low

***CustomerGenre*** - CustomerGenre class to structure the data needed to return a custumer with their most populer genre

#### Reposotories
***ConnectionStringHelper*** - Connect to local database

***ICustomerReposotory*** - Interface with methods needed to recevie wanted data from datbase

***CustomerReposotory*** - Class that implements interface and fill inn methods

## Maintainers

Jostein Skadberg - @josteinskadberg 

Silja Stubhaug Torkildsen - @SiljaTorki


## Project status

Complete!

