ALTER TABLE Assistant
	 ADD SuperheroID int CONSTRAINT Assistant_Superhero_FK 
	 FOREIGN KEY REFERENCES Superhero(ID);