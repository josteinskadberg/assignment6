USE SuperherosDb
INSERT INTO Power(Name,Description) VALUES
	('Super Intelligence','Genius level of intelligence and ingenuity'),
	('Web Shooting', 'The ability to shoot spider webs from body parts'),
	('Super Strength', 'Inhuman levels of strength'),
	('Money', 'An super excess amount of hard cash money'); 

INSERT INTO SuperheroPowerRelation (SuperheroID, PowerID) VALUES 
	((SELECT ID FROM Superhero WHERE name = 'Spiderman'), (SELECT ID FROM Power WHERE name = 'Web Shooting')),
	((SELECT ID FROM Superhero WHERE name = 'Spiderman'), (SELECT ID FROM Power WHERE name = 'Super Strength')),
	((SELECT ID FROM Superhero WHERE name = 'Batman'), (SELECT ID FROM Power WHERE name = 'Money')),
	((SELECT ID FROM Superhero WHERE name = 'Ironman'), (SELECT ID FROM Power WHERE name = 'Money')),
	((SELECT ID FROM Superhero WHERE name = 'Ironman'), (SELECT ID FROM Power WHERE name = 'Super Intelligence')),
	((SELECT ID FROM Superhero WHERE name = 'Batman'), (SELECT ID FROM Power WHERE name = 'Super Intelligence')); 