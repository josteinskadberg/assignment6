﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryImplementation.RepositoryImplementation.Model
{
    /// <summary>
    ///     CustomerGenre class to structure the data needed to return a custumer with their most populer genre
    /// </summary>
    public class CustomerGenre
    {
        public int CustomerId { get; set; }
        public string Name { get; set; }
        public int GenreCount { get; set; }
    }
}
