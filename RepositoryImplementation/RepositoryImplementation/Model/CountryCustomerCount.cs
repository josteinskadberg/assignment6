﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryImplementation.RepositoryImplementation.Model
{
    /// <summary>
    ///     CountryCustomerCount class to structure the data needed to return number of customers in each cuntry
    /// </summary>
    public class CountryCustomerCount
    {
        public int CustomerCount { get; set; }
        public string Country { get; set; }
    }
}
