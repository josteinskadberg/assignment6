﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryImplementation.RepositoryImplementation.Model
{
    /// <summary>
    ///     CustomerSpend class to structure the data needed to retrun the custumers who are highest spenders from high to low
    /// </summary>
    public class CustomerSpend
    {
        public int CustomerId { get; set; }
        public decimal Total { get; set; }
    }
}
