﻿using Microsoft.Data.SqlClient;
using RepositoryImplementation.RepositoryImplementation.Model;
using SQLDataBaseServerDemo.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryImplementation.RepositoryImplementation.Reposotories
{
    /// <summary>
    /// CustomerReposotory class who implements the interface with methods that interact with tha database
    /// </summary>
    public class CustomerReposotory : ICustomerReposotory
    {

        /// <summary>
        /// Method to get all Customers from the database
        /// </summary>
        /// <returns> List of Customer objects </returns>
        public List<Customer> GetAllCustomers()
        {
            //Query that get wanted data from database
            String query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";
            List<Customer> result = new(); 
            try
            {
                //Conntect to database 
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    //Command to execute against a SQL Server database
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        //Read data from the SQL Server database
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = createCustomer(reader);
                                result.Add(temp); 

                            }
                        }
                    }

                }
            }
            catch (SqlException err) //Handle error
            {
                Console.WriteLine(err.Message);

            }
            return result; 
        }

        /// <summary>
        ///     Method to get all the custumers from the database, 
        ///     will return the custumers who has a name that matches the name given in the parameter of the method
        /// </summary>
        /// <param name="Name"></param>
        /// <returns> List of Customer objects </returns>
        public List<Customer> GetCustomerByname(string Name)
        {
            //Query that get wanted data from database
            String query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " + "WHERE FirstName = @FirstName";
            List<Customer> result = new();

            try
            {
                //Conntect to database 
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    //Command to execute against a SQL Server database 
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", Name);

                        //Read data from the SQL Server database
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result.Add(createCustomer(reader));
                            }
                        }
                    }

                }
            }
            catch (SqlException err) //Handle error
            {
                Console.WriteLine(err.Message);

            }
            return result;
        }

        /// <summary>
        ///     Method to get one Customer from the database, 
        ///     will return the Customer that has the same ID as given in the paramater of the method
        /// </summary>
        /// <param name="id"></param>
        /// <returns> Customer object</returns>
        public Customer GetCustumerByID(string id) 
        {
            //Query that get wanted data from database
            String query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer " +"WHERE CustomerId = @CustomerId";
            Customer result = new(); 
        
            try
            {
                //Conntect to database
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    //Command to execute against a SQL Server database 
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerID", id);

                        //Read data from the SQL Server database
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result = createCustomer(reader);

                            }
                        }
                    }

                }
            }
              catch (SqlException err) 
                {
                    Console.WriteLine(err.Message);

                }

                return result; 
                       
       }

        /// <summary>
        ///     Help-method to create new customer
        /// </summary>
        /// <param name="reader"></param>
        /// <returns> Customer object </returns>
        private static Customer createCustomer(SqlDataReader reader)
        {
            return new Customer
            {
                CustomerId = reader.GetInt32(0),
                FirstName = reader.GetString(1),
                LastName = reader.GetString(2),
                Country = reader.GetString(3),
                PostalCode = SqlReader.SafeGetString(reader,4),
                Phone = SqlReader.SafeGetString(reader,5),
                Email = reader.GetString(6)
            };
        }

        /// <summary>
        ///     Method to get a subset of the database to create a page of customers
        ///     The parameters limit and offset is used to make the subset
        ///     The offset argument is used to identify the starting point 
        ///     The limit argument is used to specify the number of records
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns> List of Customer objects</returns>
        public List<Customer> GetPageOfCustomers(int limit, int offset)
        {
            string query = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                "FROM Customer " +
                "ORDER BY CustomerId OFFSET @Offset ROWS FETCH NEXT @Limit ROWS ONLY;";
            
            List<Customer> result = new();
            
            try
            {
                //Conntect to database
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    //Command to execute against a SQL Server database
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@Offset", offset);
                        cmd.Parameters.AddWithValue("@Limit", limit);

                        //Read data from the SQL Server database
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                result.Add(createCustomer(reader));
                            }
                        }
                    }
                }
            }
            catch (SqlException err) 
            {
                Console.WriteLine(err.Message);
            }
            return result;
        }

        /// <summary>
        ///     Mathod to add a new customer to database
        /// </summary>
        /// <param name="customer"></param>
        /// <returns> Boolean </returns>
        public bool InsertCustomer(Customer customer)
        {
            bool success = false;

            //Query that get wanted data from database
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email)" +
                "VALUES(  @FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            try
            {
                //Conntect to database 
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    //Command to execute against a SQL Server database 
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Add value to customer
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        //If succesfull set true, if nott set false
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException err)
            {
                Console.WriteLine(err.Message);

            }
            return success;
        }

        /// <summary>
        ///     Method to update an existig custumer in the database
        /// </summary>
        /// <param name="customer"></param>
        /// <param name="id"></param>
        /// <returns> Boolean </returns>
        public bool UpdateCutomer(Customer customer, string id)
        {
            bool success = false;

            //Query that get wanted data from database
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, " +
                "PostalCode = @PostalCode, Phone = @Phone, Email = @Email" +
                " WHERE CustomerId =  @CustomerId"; 
            try
            {
                //Conntect to database
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    //Command to execute against a SQL Server database 
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        //Add value to customer
                        cmd.Parameters.AddWithValue("@CustomerId", id); 
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        //If succesfull set true, if nott set false
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException err)
            {
                Console.WriteLine(err.Message);

            }

            return success;
        }

        /// <summary>
        ///     Method to count number of custumers in each country
        /// </summary>
        /// <returns> List of Country and count</returns>
        public List<CountryCustomerCount> GetCountryCustomerCounts()
        {
            //Query that get wanted data from database
            String query = "SELECT Country, COUNT(CustomerId) as CustomerCount FROM Customer" +
                " GROUP BY Country  ORDER BY CustomerCount DESC";
            
            List<CountryCustomerCount> result = new();

            try
            {
                //Conntect to database 
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    //Command to execute against a SQL Server database
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        //Read data from the SQL Server database
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            //While there is a next record to read
                            while (reader.Read())
                            {
                                // Handle Result read from database
                                CountryCustomerCount count = new CountryCustomerCount();
                                count.Country = reader.GetString(0);
                                count.CustomerCount = reader.GetInt32(1);
                                //Add values to empty list
                                result.Add(count);

                            }
                        }
                    }

                }
            }
            catch (SqlException err)
            {
                Console.WriteLine(err.Message);

            }

            return result;
        }

        /// <summary>
        ///     Method to find the highest spending customer.
        ///     List orderd form highets spending to lowest.
        /// </summary>
        /// <returns> List of CustomerID and Total money spend for customer </returns>
        public List<CustomerSpend> GetHighetsSpending()
        {
            //Query that get wanted data from database
            String query = "SELECT Customer.CustomerId, Sum(Invoice.Total)" +
                " as totalSpent FROM Customer INNER JOIN Invoice ON Customer.CustomerId  = Invoice.CustomerID" +
                " GROUP BY Customer.CustomerId" +
                " Order by  totalSpent  DESC";
            
            List<CustomerSpend> result = new();

            try
            {
                //Conntect to database
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();

                    //Command to execute against a SQL Server database
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        //Read data from the SQL Server database
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            //While there is a next record to read
                            while (reader.Read())
                            {
                                // Handle Result read from database
                                CustomerSpend spend = new CustomerSpend();
                                spend.CustomerId = reader.GetInt32(0);
                                spend.Total = reader.GetDecimal(1);
                                //Add values to the empty List
                                result.Add(spend);

                            }
                        }
                    }

                }
            }
            catch (SqlException err) //Handle error
            {
                Console.WriteLine(err.Message);

            }

            //Return List
            return result;
        }

        /// <summary>
        ///     Method to find the most popular Genre for given custumer
        ///     Given customer is find by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns> List of Customer with the most popular Genre</returns>
        public List<CustomerGenre> GetCustomerGenres(string id)
        {
            //SQL Query that get wanted data from database
            String query = "SELECT TOP 1 WITH TIES Customer.CustomerId, Genre.Name, Count(Genre.Name) AS GenreCount" +
                " FROM Customer" +
                " INNER JOIN Invoice ON Customer.CustomerId  = Invoice.CustomerID" +
                " INNER JOIN InvoiceLine ON Invoice.InvoiceId = InvoiceLine.InvoiceId" +
                " INNER JOIN Track ON InvoiceLine.TrackId = Track.TrackId" +
                " INNER JOIN Genre ON Track.GenreId = Genre.GenreId" +
                " WHERE Customer.CustomerId = @CustomerId" +
                " GROUP BY Customer.CustomerId, Genre.Name" +
                " ORDER BY  GenreCount  DESC";
            
            // Empty List
            List<CustomerGenre> result = new();

            try
            {
                //Conntect to database 
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();//Opens database connection

                    //Command to execute against a SQL Server database
                    using (SqlCommand cmd = new SqlCommand(query, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerID", id);

                        //Read data from the SQL Server database
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre genre = new CustomerGenre();
                                genre.CustomerId = reader.GetInt32(0);
                                genre.Name = reader.GetString(1);
                                genre.GenreCount = reader.GetInt32(2);
                                result.Add(genre);

                            }
                        }
                    }
                }
            }
            catch (SqlException err) 
            {
                Console.WriteLine(err.Message);

            }
            return result;
        }
    }
}
