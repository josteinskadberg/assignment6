﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryImplementation.RepositoryImplementation.Reposotories
{
    /// <summary>
    /// Help class that provides a way of reading a forward-only stream
    /// of rows from a SQL Server database
    /// </summary>
    public static class SqlReader
    {
        /// <summary>
        ///     Method to get information from database,
        ///     if there is nothing to read it returns null
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="colIndex"></param>
        /// <returns> String </returns>
        public static string SafeGetString(this SqlDataReader reader, int colIndex)
        {
            //If the reader is not null, return reader
            if (!reader.IsDBNull(colIndex))
                return reader.GetString(colIndex);
            return "NULL";
        }
    }
}
