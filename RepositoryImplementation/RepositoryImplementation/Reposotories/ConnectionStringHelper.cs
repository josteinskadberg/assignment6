using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLDataBaseServerDemo.Repositories
{
    /// <summary>
    /// Class created to help connect to the database
    /// </summary>
    class ConnectionStringHelper
    {
        /// <summary>
        /// Static method that build a connection string
        /// </summary>
        /// <returns> String </returns>
        public static string GetConnectionString()
        {
            // new SqlConnectionStringBuilder object
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();

            // Set the name of the SQL server 
            connectionStringBuilder.DataSource = "DESKTOP-HUT8JA8\\SQLEXPRESS";
            // Set the name of the database 
            connectionStringBuilder.InitialCatalog = "Chinook";
            // The current Windows account credentials are used for authentication
            connectionStringBuilder.IntegratedSecurity = true;
            //The transport layer will use SSL to encrypt the channel and bypass walking the certificate chain to validate trust
            connectionStringBuilder.TrustServerCertificate = true; 

            //Return the connection string
            return connectionStringBuilder.ConnectionString; 
        }
    }
}

