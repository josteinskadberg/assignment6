﻿using RepositoryImplementation.RepositoryImplementation.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryImplementation.RepositoryImplementation.Reposotories
{
    /// <summary>
    /// Interface constaining methods to interact whit database
    /// </summary>
    public interface ICustomerReposotory
    {
        // Get all customers
        public List<Customer>  GetAllCustomers();
        
        // Get one customer by ID
        public Customer GetCustumerByID(string id);

        // Get custumers by Name
        public List<Customer> GetCustomerByname(string Name);

        // Get a page of cusomers created with a subset of the database
        public List<Customer> GetPageOfCustomers(int limit, int offset);

        // Add a new customer
        public bool InsertCustomer(Customer customer);

        // Update a custmer
        public bool UpdateCutomer(Customer customer, string id);

        // Get number of custumer in each cuntry
        public List<CountryCustomerCount> GetCountryCustomerCounts();

        // Get the custumers who are highest spenders
        public List<CustomerSpend> GetHighetsSpending();

        // Get the most popular genre for given customer
        public List<CustomerGenre> GetCustomerGenres(string id);

    }
}
