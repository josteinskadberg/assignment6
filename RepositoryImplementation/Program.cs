﻿using RepositoryImplementation.RepositoryImplementation.Model;
using RepositoryImplementation.RepositoryImplementation.Reposotories;
using System;
using System.Collections.Generic;

namespace RepositoryImplementation
{
    class Program
    {
        static void Main(string[] args)
        {
            // New customer - TEST DATA
            Customer newCustomer = new()
            {
                FirstName = "SiljaTest",
                LastName = "Torkilsen",
                Country = "Norway",
                PostalCode = "5033",
                Email = "ItsSilje@outlook.com",
                Phone = "+47 99872343"
            };

            //Update exixting customer - TEST DATA
            Customer updatedCustomer = new()
            {
                FirstName = "Silja",
                LastName = "Torkilsen",
                Country = "Norway",
                PostalCode = "5033",
                Email = "SiljaGotNewMail@update.com",
                Phone = "+47 99872343"
            };


            //Creating a new repository
            CustomerReposotory repo = new();

            //Get customer by ID - Test ID = 1
            Customer customerByID = repo.GetCustumerByID("1");

            //Get customer by name - Test Name = Frank
            List<Customer> customerByName = repo.GetCustomerByname("Frank");

            //Get all customers
            List<Customer> allCustomers = repo.GetAllCustomers();

            //Get page of customers, Test- limit = 5, offset = 10
            List<Customer> customerPage = repo.GetPageOfCustomers(5,10);

            //Get all Countries where the customers are from,
            //the country most customers live in will be listed first
            List<CountryCustomerCount>  customersPerCountry = repo.GetCountryCustomerCounts();
            
            //Get a list of custoumers,
            //where the higest spending is listed first and the lowest spending listed last
            List<CustomerSpend> highestSpedningCustomers = repo.GetHighetsSpending();
            
            //Get the most populare Genres to given customer - Test customer with ID = 12
            List<CustomerGenre> mostpopularGenre = repo.GetCustomerGenres("12");

            //Boolean that check if new customer was added to database
            bool inserted = repo.InsertCustomer(newCustomer);
            //Get customers by name and add all to a list
            List<Customer> SiljaTest = repo.GetCustomerByname("SiljaTest");
            Console.WriteLine("Inserted new Customer: ");
            PrintCustomer(SiljaTest[0]);

            //Boolean that check if an existing customer was updated
            bool updated = repo.UpdateCutomer(updatedCustomer, SiljaTest[0].CustomerId.ToString());
            //Set Customer to the customer that match with given ID
            Customer SiljaUpdated = repo.GetCustumerByID(SiljaTest[0].CustomerId.ToString());
            Console.WriteLine("\nUpdated Customer:");
            PrintCustomer(SiljaUpdated);

            //Prints customer by ID - Test ID = 1
            Console.WriteLine("\nCustomer By ID (1):");
            PrintCustomer(customerByID);

            //Prints all customers that has the same name
            Console.WriteLine("\nCustomer By Name: ");
            foreach (var customer in customerByName)
            {
                PrintCustomer(customer); 
            }

            //Prints all customers
            Console.WriteLine("\n All Customers: ");
            foreach (var customer in allCustomers)
            {
                PrintCustomer(customer);
            }

            //Print page of customers, Test- limit = 5, offset = 10
            Console.WriteLine("\n Page of Customers: ");
            foreach (var customer in customerPage)
            {
                PrintCustomer(customer);
            }

            //Print all Countries where the customers are from,
            //the country most customers live in will be listed first
            Console.WriteLine("\n Countries with most Customers: ");
            foreach (var country in customersPerCountry)
            {
                PrintCountCustomerCountry(country); 
            }

            //Prints all custoumers,
            //where the higest spending is listed first and the lowest spending listed last
            Console.WriteLine("\n Higihest spending Customers: ");
            foreach (var customer in highestSpedningCustomers)
            {
                PrintCustomerSpend(customer); 
            }

            //Prints the most populare Genres to customer with ID = 12
            Console.WriteLine("\n Customer Most Popular Genre: ");
            foreach (var genre in mostpopularGenre)
            {
                PrintCustomerGenre(genre);
            }


        }

        /// <summary>
        ///     Print method to print a customer object
        /// </summary>
        /// <param name="customer"></param>
        public static void PrintCustomer(Customer customer)
        { 
            Console.WriteLine($"--ID: {customer.CustomerId} FirstName: {customer.FirstName}\tLastName: " +
                $"{customer.LastName}\tEmail: {customer.Email}\tPhone:{customer.Phone}\tCountry: {customer.Country}\t" +
                $"PostalCode: {customer.PostalCode}--" );
        }

        /// <summary>
        ///     Print method to print Cuntry and the number of how many customers that lives there
        /// </summary>
        /// <param name="countryCustomerCount"></param>
        public static void PrintCountCustomerCountry(CountryCustomerCount countryCustomerCount)
        {
            Console.WriteLine($"-- Country: { countryCustomerCount.Country} Count: {countryCustomerCount.CustomerCount} -- ");
        }

        /// <summary>
        ///     Print method to print the Customer ID and how mutch money they spend in total
        /// </summary>
        /// <param name="customerSpend"></param>
        public static void PrintCustomerSpend(CustomerSpend customerSpend)
        {
            Console.WriteLine($"-- CustomerId: { customerSpend.CustomerId} Total: {customerSpend.Total} -- ");
        }

        /// <summary>
        ///     Print method that print the customers ID, Name of most popular Genre and the total count of the Genre
        /// </summary>
        /// <param name="customerGenre"></param>
        public static void PrintCustomerGenre(CustomerGenre customerGenre)
        {
            Console.WriteLine($"--  CustomerId: { customerGenre.CustomerId } Name { customerGenre.Name } GenreCount {customerGenre.GenreCount} --");
        }
    }
}
